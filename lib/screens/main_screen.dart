import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geofence_demo_app/app_localizations.dart';
import 'package:geofence_demo_app/mixins/tools.dart';
import 'package:geofence_demo_app/utils/notifier.dart';

class MainScreen extends StatefulWidget with Tools {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with Tools {
  bool _loading = false;
  Notifier notifier;

  @override
  void initState() {
    super.initState();
    notifier = new Notifier();
    notifier.getLaunchDetails().then((value) {
      if (value) {
        Navigator.pushNamed(context, '/geofence');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return (_loading == false)
        ? Scaffold(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            body: Container(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: getSizeByPercentage(screenSize.width, 10),
                      vertical: getSizeByPercentage(screenSize.height, 35),
                    ),
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                          child: Image(
                            image: AssetImage(
                                'assets/images/geofence_demo_logo.png'),
                          ),
                          onPressed: () {
                            Navigator.of(context).pushNamed('/geofence');
                          },
                        ),
                        Text(AppLocalizations.of(context)
                            .translate('tap_to_enter')),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: getSizeByPercentage(screenSize.width, 50),
                      top: getSizeByPercentage(screenSize.height, 90),
                    ),
                    child: FlatButton.icon(
                      icon: Icon(Icons.settings),
                      label: Text('Configure geofences'),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/config');
                      },
                    ),
                  ),
                ],
              ),
            ),
            resizeToAvoidBottomInset: false,
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }
}
