import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geofence_demo_app/blocs/data/data_bloc_index.dart';
import 'package:geofence_demo_app/mixins/tools.dart';
import 'package:geofence_demo_app/widgets/geofence_indicator_box.dart';

class GeofenceScreen extends StatefulWidget {
  @override
  _GeofenceScreenState createState() => _GeofenceScreenState();
}

class _GeofenceScreenState extends State<GeofenceScreen> with Tools {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final bloc = BlocProvider.of<DataBloc>(context);

    return BlocBuilder<DataBloc, DataState>(
      builder: (context, state) {
        if (state is DataLoading) {
          bloc.dispatch(LoadData());
          return Center(child: CircularProgressIndicator());
        } else {

          bool isInside = _isInsideGeofence(state);

          return Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).appBarTheme.color,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getSizeByPercentage(screenSize.width, 15),
                  vertical: getSizeByPercentage(screenSize.height, 20),
                ),
                child: GeofenceIndicatorBox(
                  isInside,
                  getSizeByPercentage(screenSize.height, 40),
                  getSizeByPercentage(screenSize.width, 70),
                  getSizeByPercentage(screenSize.height, 2),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  bool _isInsideGeofence(dynamic state){
    if(state.data['inside'] != null){
      return state.data['inside'];
    } 

    return false;
  }
}
