import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_geofencing/flutter_geofencing.dart';
import 'package:geofence_demo_app/app_localizations.dart';
import 'package:geofence_demo_app/blocs/data/data_bloc_index.dart';
import 'package:geofence_demo_app/mixins/validator.dart';
import 'package:geofence_demo_app/utils/notifier.dart';
import 'package:geofence_demo_app/widgets/custom_slider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigScreen extends StatefulWidget {
  @override
  _ConfigScreenState createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> with Validator {
  final _formKey = GlobalKey<FormState>();
  final _latController = TextEditingController();
  final _lonController = TextEditingController();
  final _radiusController = TextEditingController();
  ReceivePort port = ReceivePort();
  final List<GeofenceEvent> triggers = <GeofenceEvent>[
    GeofenceEvent.enter,
    GeofenceEvent.exit,
  ];
  final AndroidGeofencingSettings androidSettings =
      AndroidGeofencingSettings(initialTrigger: <GeofenceEvent>[
    GeofenceEvent.enter,
    GeofenceEvent.exit,
  ], loiteringDelay: 1000 * 60);

  @override
  void initState() {
    super.initState();
    IsolateNameServer.registerPortWithName(
        port.sendPort, 'geofencing_send_port');
    port.listen((dynamic data) {
      print('Event: $data');
    });
    initPlatformState();

    _latController.text = '0.0';
    _lonController.text = '0.0';
    _radiusController.text = '100';
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<DataBloc>(context);

    return BlocBuilder<DataBloc, DataState>(
      builder: (context, state) {
        if (state is DataLoading) {
          bloc.dispatch(LoadData());
          return Center(child: CircularProgressIndicator());
        } else {
          _checkSettings(state);

          return Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).appBarTheme.color,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: Builder(
              builder: (context) => Padding(
                padding: const EdgeInsets.all(30),
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            controller: _latController,
                            decoration: InputDecoration(labelText: 'lat'),
                            validator: (value) => validate(
                              context,
                              [Validator.isRequired, Validator.isDouble],
                              value,
                            ),
                          ),
                          TextFormField(
                            controller: _lonController,
                            decoration: InputDecoration(labelText: 'lon'),
                            validator: (value) => validate(
                              context,
                              [Validator.isRequired, Validator.isDouble],
                              value,
                            ),
                          ),
                          CustomSlider(
                            values: [double.parse(_radiusController.text)],
                            onDragCompleted: (double value) {
                              _radiusController.text = value.toString();
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton.icon(
                        color: Theme.of(context).buttonColor,
                        icon: Icon(Icons.add),
                        label: Text(AppLocalizations.of(context).translate('add_geofence')),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _registerGeofenceRegion(context);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  void _checkSettings(dynamic state) {
    print(state.data);
    if (state.data['latitude'] != null) {
      _latController.text = state.data['latitude'].toString();
    }
    if (state.data['longitude'] != null) {
      _lonController.text = state.data['longitude'].toString();
    }
    if (state.data['radius'] != null) {
      _radiusController.text = state.data['radius'].toString();
    }
  }

  void _registerGeofenceRegion(BuildContext context) {
    _commitChanges();

    GeofencingManager.removeGeofenceById('region1');

    GeofencingManager.registerGeofence(
        GeofenceRegion(
            'region1',
            double.parse(_latController.text),
            double.parse(_lonController.text),
            double.parse(_radiusController.text),
            triggers,
            androidSettings: androidSettings),
        callback);
    final snackBar = SnackBar(
      content: Text(
        AppLocalizations.of(context).translate('geofence_added'),
      ),
      elevation: 6.0,
      backgroundColor: Colors.green,
      duration: Duration(seconds: 1),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _commitChanges() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble('latitude', double.parse(_latController.text));
    prefs.setDouble('longitude', double.parse(_lonController.text));
    prefs.setDouble('radius', double.parse(_radiusController.text));
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    print('Initializing...');
    await GeofencingManager.initialize();
    print('Initialization done');
  }

  //Geofence event callback
  static void callback(List<String> ids, Location l, GeofenceEvent e) async {
    print('Fences: $ids Location $l Event: $e');
    final SendPort send =
        IsolateNameServer.lookupPortByName('geofencing_send_port');
    send?.send(e.toString());

    //saving changes
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (e.toString().contains('enter')) {
      prefs.setBool('inside', true);
    } else {
      prefs.setBool('inside', false);
    }

    //scheduling notification
    try{
    Notifier notifier = new Notifier();
    notifier.scheduleNotification(DateTime.now());
    }catch(e) {
      print('Notif Error: $e');
    }
  }
}
