import 'package:flutter/cupertino.dart';
import 'package:geofence_demo_app/app_localizations.dart';

/// Class that handles fields validation
mixin Validator {

  /// `String` property to tell Validator to check field as required
  static const String isRequired = 'required';

  /// `String` property to tell Validator to check field as numeric
  static const String isNumeric = 'numeric';

  /// `String` property to tell Validator to check field as integer
  static const String isInteger = 'int';

  /// `String` property to tell Validator to check field as double
  static const String isDouble = 'double';

  /// `String` property to tell Validator to check field as greater than zero
  static const String isGreaterThanZero = 'greaterThanZero';

  /// Validates a `TextField`
  /// Returns `null` if everything is OK or a `String` with the error.
  /// Expects a `BuildingContext` 'context', a `List<String>` 'validations'
  /// (Use static properties from this class to fill this parameter),
  /// and a `String` 'value'
  validate(BuildContext context, List<String> validations, String value) {
    var result;
    
    for(String validation in validations){  
        switch (validation) {
          case isRequired:
            result = isRequiredFunction(context, value);
            break;
          case isNumeric:
            result = isNumericFunction(context, value);
            break;
          case isInteger:
            result = isIntFunction(context, value);
            break;
          case isDouble:
            result = isDoubleFunction(context, value);
            break;
          case isGreaterThanZero:
            result = isGreaterThanZeroFunction(context, value);
            break;
          default:
            throw Exception(
                'Not valid validation... Check this class properties for available validations');
            break;
        }

        if(result != null){
          break;
        }
        
      }
      return result;
  }

  /// Validates a `TextField`
  /// Returns `null` if everything is OK or a `String` with the error.
  /// Expects a `BuildingContext` 'context' and a `String` 'value'
  isRequiredFunction(BuildContext context, String value) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context).translate('field_validator_required');
    } else {
      return null;
    }
  }

  /// Validates if a `TextField` value is Numeric (int or double)
  /// Returns `null` if everything is OK or a `String` with the error
  /// Expects a `BuildingContext` 'context' and a `String` 'value'
  isNumericFunction(BuildContext context, String value) {
    var result = isIntFunction(context, value);
    if (result != null) {
      return isDoubleFunction(context, value);
    } else {
      return result;
    }
  }

  /// Validates if a `TextField` value is Numeric (int)
  /// Returns `null` if everything is OK or a `String` with the error
  /// Expects a `BuildingContext` 'context' and a `String` 'value'
  isIntFunction(BuildContext context, String value) {
    try {
      int.parse(value);
      return null;
    } on FormatException {
      return AppLocalizations.of(context).translate('field_validator_numeric');
    }
  }

  /// Validates if a `TextField` value is Numeric (int)
  /// Returns `null` if everything is OK or a `String` with the error
  /// Expects a `BuildingContext` 'context' and a `String` 'value'
  isDoubleFunction(BuildContext context, String value) {
    try {
      double.parse(value);
      return null;
    } on FormatException {
      return AppLocalizations.of(context).translate('field_validator_numeric');
    }
  }

  /// Validates if a `TextField` value is Numeric (int or double) and is greater than zero
  /// Returns `null` if everything is OK or a `String` with the error
  /// Expects a `BuildingContext` 'context' and a `String` 'value'
  isGreaterThanZeroFunction(BuildContext context, String value) {
    var resultInt = isIntFunction(context, value);
    var resultDouble = isDoubleFunction(context, value);

    if (resultInt == null) {
      return (int.parse(value) > 0)
          ? null
          : AppLocalizations.of(context)
              .translate('field_validator_greater_zero');
    } else if (resultDouble == null) {
      return (double.parse(value) > 0)
          ? null
          : AppLocalizations.of(context)
              .translate('field_validator_greater_zero');
    } else {
      return resultInt;
    }
  }
}
