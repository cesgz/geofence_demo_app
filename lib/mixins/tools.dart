mixin Tools {

  /// Returns a `double` that represents the correct size to use in flutter.
  /// Expects a `double` 'parentSize' (Parent container size) and a `double` 'percentage'
  /// (Percentage % obtained from any design tool, it could be the same percentage used on CSS)
  double getSizeByPercentage(double parentSize, double percentage) {
    return parentSize * (percentage / 100);
  }
}
