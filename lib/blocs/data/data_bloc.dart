import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './data_event.dart';
import './data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  @override
  DataState get initialState => DataLoading();

  @override
  Stream<DataState> mapEventToState(DataEvent event) async* {
    if (event is LoadData) {
      yield DataLoading();
      yield* _reloadData();
    }
  }

  Stream<DataState> _reloadData() async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double latitude = prefs.getDouble('latitude');
    double longitude = prefs.getDouble('longitude');
    double radius = prefs.getDouble('radius');
    bool inside = prefs.getBool('inside');
    yield DataLoaded({
      "latitude": latitude,
      "longitude": longitude,
      "radius": radius,
      "inside": inside,
    });
  }
}
