import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DataState extends Equatable {
  DataState([List props = const []]) : super(props);
}

class DataLoading extends DataState {}

class DataLoaded extends DataState {
  final Map<String, dynamic> data;

  DataLoaded(this.data) : super([data]);
}