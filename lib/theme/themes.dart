import 'package:flutter/material.dart';

final ThemeData themeData = ThemeData(
  appBarTheme: AppBarTheme(
    color: Colors.grey.shade500,
  ),
  buttonColor: Colors.grey.shade500,
  scaffoldBackgroundColor: Colors.white
);
