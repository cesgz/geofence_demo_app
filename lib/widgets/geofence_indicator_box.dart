import 'package:flutter/material.dart';
import 'package:geofence_demo_app/app_localizations.dart';

class GeofenceIndicatorBox extends StatelessWidget {
  
  final bool inside;
  final double height, width, textTopPadding;

  GeofenceIndicatorBox(
    this.inside,
    this.height,
    this.width,
    this.textTopPadding,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: height,
          width: width,
          color: (inside) ? Colors.green : Colors.red,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: textTopPadding,
          ),
          child: Text(
            (inside)
                ? AppLocalizations.of(context).translate('inside_geofence')
                : AppLocalizations.of(context).translate('outside_geofence'),
          ),
        ),
      ],
    );
  }
}
