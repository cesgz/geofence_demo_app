import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:geofence_demo_app/app_localizations.dart';

// Definition for [DragCompletedCallBack]
typedef DragCompletedCallBack = void Function(double value);

class CustomSlider extends StatelessWidget {
  final DragCompletedCallBack onDragCompleted;
  final List<double> values;

  CustomSlider({this.onDragCompleted, this.values});

  @override
  Widget build(BuildContext context) {
    return FlutterSlider(
      values: values,
      onDragCompleted: (handlerIndex, lowerValue, upperValue) =>
          onDragCompleted(lowerValue),
      max: 1000,
      min: 100,
      tooltip: FlutterSliderTooltip(
        rightSuffix: Text(' ${AppLocalizations.of(context).translate('meters')}'),
      ),
      step: 10,

      hatchMark: FlutterSliderHatchMark(
        labels: [
          FlutterSliderHatchMarkLabel(
            label: Text('100M'),
            percent: 1,
          ),
          FlutterSliderHatchMarkLabel(
            label: Text('1000M'),
            percent: 100,
          ),
        ],
      ),
    );
  }
}
