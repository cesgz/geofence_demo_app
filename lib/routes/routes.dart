import 'package:flutter/material.dart';
import 'package:geofence_demo_app/screens/main_screen.dart';
import 'package:geofence_demo_app/screens/geofence_screen.dart';
import 'package:geofence_demo_app/screens/config_screen.dart';

class Routes {
  Routes._();

    static final routes = <String, WidgetBuilder>{
    '/': (BuildContext context) => MainScreen(),
    '/geofence': (BuildContext context) => GeofenceScreen(),
    '/config': (BuildContext context) => ConfigScreen(),
  };
}

