import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// Class that handles notifications scheduling
class Notifier {
  final _channelId = 'GeofenceDemoChannel';
  final _channelName = 'GeofenceDemoChannel';
  final _channelDescription = 'Channel Geofencing notifications';
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  bool _initialized = false;

  static final Notifier _singleton = Notifier._internal();

  factory Notifier() {
    if (!_singleton._initialized) {
      _singleton.initialize();
    }
    return _singleton;
  }

  Notifier._internal();

  void initialize() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onSelectNotificationIOS);
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotificationAndroid);

    _initialized = true;
  }

  // Schedules a notification at desired time
  void scheduleNotification(
    DateTime dateTime, {
    int id = 0,
    String title = 'Geofence App Notification',
    String description = '',
    String payload = '',
  }) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        _channelId, _channelName, _channelDescription);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
      id,
      title,
      description,
      dateTime,
      platformChannelSpecifics,
      androidAllowWhileIdle: true,
      payload: payload,
    );
  }

  // On notification selected callback (Android)
  Future onSelectNotificationAndroid(String payload) async {}

  // On notification selected callback (IOS)
  Future onSelectNotificationIOS(
      int integer, String string1, String string2, String string3) async {
    if (string1 != null) {
      debugPrint('android notification payload: ' + string1);
    }
  }

  // Returns true if the app was launched by a notification, returns false if it wasn't.
  Future<bool> getLaunchDetails() async {
    bool result = false;
    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then(
      (NotificationAppLaunchDetails details) {
        result = details.didNotificationLaunchApp;
      },
    );

    return result;
  }
}
